package com.question2;

public class SmartTelephone extends Telephone {

	@Override
	void lift() {
		System.out.println("Lift method");
	}

	@Override
	void disconnected() {
		System.out.println("Disconnected method");
	}

	public static void main(String[] args) {
		SmartTelephone s = new SmartTelephone();
		s.lift();
		s.disconnected();
	}
}
