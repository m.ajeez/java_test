package com.question2;

public abstract class Shape {
	abstract void SquareArea(int side);

	abstract void CircleArea(double radius);

	abstract void Rectanglearea(int length, int breadth);

}
