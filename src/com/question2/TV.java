package com.question2;

public class TV implements TVremote {

	@Override
	public void smart() {
		System.out.println("Smart method");
	}

	@Override
	public void remote() {
		System.out.println("Remote method");
	}

	public static void main(String[] args) {
		TV t = new TV();
		t.smart();
		t.remote();
	}
}
