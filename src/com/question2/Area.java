package com.question2;

public class Area extends Shape {

	@Override
	void Rectanglearea(int length, int breadth) {
		int area = length * breadth;
		System.out.println("area of rectangle= " + area);
	}

	@Override
	void SquareArea(int side) {
		int area = side * side;
		System.out.println("area of square= " + area);
	}

	@Override
	void CircleArea(double radius) {
		double area = 3.14 * radius * radius;
		System.out.println("area of circle= " + area);
	}

	public static void main(String[] args) {
		Area a = new Area();
		a.Rectanglearea(2, 4);
		a.SquareArea(3);
		a.CircleArea(4);
	}

}
