package com.question2;

public class FirstLast {
	public static void main(String[] args) {
		String s = "Hello, World";

		int indexOf = s.indexOf('o');
		System.out.println("First occurence of (o)= " + indexOf);

		int lastIndexOf = s.lastIndexOf('o');
		System.out.println("Last occurence of (o)= " + lastIndexOf);

		int indexOf2 = s.indexOf(',');
		System.out.println("First occurence of (,)= " + indexOf2);

		int lastIndexOf2 = s.lastIndexOf(',');
		System.out.println("Last occurence of (,)= " + lastIndexOf2);

	}
}
